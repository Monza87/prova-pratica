using NUnit.Framework;

namespace Tests
{
    public class Tests
    {

[Test]
        public void CPF()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CPF("02244795625"));
        }

        [Test]
        public void CNPJ()
        {
            
            Assert.AreEqual(true, ProvaPratica.Validador.CNPJ("28118883000159"));
        }
    }

}